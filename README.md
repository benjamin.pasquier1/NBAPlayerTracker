# NBAPlayerTracker

__Membres de l'équipe__ : Bourquenoud Nathan, Hirschi Laurent et Pasquier Benjamin

La National Basketball Association a été créée en 1946, sous le nom de Basketball Association of America avant d'être renommée en 1949. Jusqu'à aujourd'hui, cette ligue a évolué dans de nombreux aspects, les attentes des entraîneurs ou des directeurs sportifs ont donc également changé. Par exemple, l'arrivée de la ligne à trois points dans les années 80 a changé le jeu à jamais, au point que l'habilité des joueurs aux tirs à trois points demeure aujourd'hui une des compétences les plus recherchées. Les salaires ont également augmenté drastiquement et il est aujourd'hui difficile de juger si un joueur est payé de manière adéquate par rapport à ses performances. Ce projet a donc pour but de visualiser les caractéristiques des joueurs NBA selon différents angles afin d'envisager des achats, des ventes ou des transferts qui permettraient d'améliorer l'équipe de l'entraîneur ou du directeur sportif.

## Choix des données

La NBA enregistre un très grand nombre de statistiques durant les matchs disputés mais également toutes sortes d'informations concernant les joueurs et les équipes. Ces données se trouvent sur le [site web officiel](https://www.nba.com/) de la nba et peuvent être accessibles via des APIs. Étant donné qu'il existe des sources de données plus simple à manipuler, cette API n'est pas directement utilisée. Dans le cadre de ce projet, les données sources de données suivantes sont utilisées :
* Un [dataset Kaggle](https://www.kaggle.com/datasets/wyattowalsh/basketball) contenant des données sur les matchs joués depuis 1946 ainsi que sur les joueurs et équipes de la NBA.
* Une [API Python](https://github.com/swar/nba_api) simple d'utilisation permettant d'accéder facilement aux APIs du site officiel de la NBA et donc de toutes les données qu'il contient. Cette API est utilisée pour complétée des données manquantes du dataset Kaggle.

## Technologies utilisées

Certainement Dash plotly

## Intention et public cible

Permettre à des entraîneurs et/ou des directeurs sportifs de chercher et trouver des joueurs évoluant en NBA qui correspondent au mieux à leurs attentes. La réalisation de ce projet leur permet de naviguer parmi tous les joueurs de la ligue et de les filtrer selon plusieurs critères (statistique particulière, salaire, rentabilité du joueur, performances globales) afin de trouver la(les) pièce(s) manquante(s) de leur équipe.

## Choix de représentation

## Présentation et interaction

## Critique des outils utilisés
